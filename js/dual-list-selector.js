/*!
 * JQuery lightweight plugin to build dual items selector.
 * Copyright 2018 Vinh Nguyen The, https://www.linkedin.com/in/nguyenthevinhbk/
 * Released under the MIT license
 */
(function ($) {

    $.fn.dualListSelector = function (opts) {
        // defaults options
        var me = this;
        var defaults = {
            itemsContainerHeight: "200px",
            keepOrder: true,
            multipleSelect: true,
            showHeader: false,
            availableHeader: "",
            selectedItemHeader: ""
        };
        me.opts = $.extend(defaults, opts);
        opts = me.opts;
        if (me.hasClass("dual-list-selector-initialized")) {
            return me;
        }
        var mpButtonCols = $('<div class="col col-md-1 hidden-sm-down dls-button-container"/>');
        var mpButtonRows = $('<div class="col-12 hidden-sm-down dls-button-container"/>');
        var btnToRight = $('<button type="button" class="btn btn-right-pick"><i class="fas fa-angle-right"></i></button>');
        var btnToLeft = $('<button type="button" class="btn btn-left-pick-all"><i class="fas fa-angle-left"></i></button>');
        var btnAllToRight = $('<button type="button" class="btn btn-right-pick"><i class="fas fa-angle-double-right"></i></button>');
        var btnAllToLeft = $('<button type="button" class="btn btn-left-pick"><i class="fas fa-angle-double-left"></i></button>');
        if (opts.btnToRightClassName) {
            btnToRight.addClass(opts.btnToRightClassName);
        }
        if (opts.btnAllToRightClassName) {
            btnAllToRight.addClass(opts.btnAllToRightClassName);
        }
        if (opts.btnToLeftClassName) {
            btnToLeft.addClass(opts.btnToLeftClassName);
        }
        if (opts.btnAllToLeftClassName) {
            btnAllToLeft.addClass(opts.btnAllToLeftClassName);
        }
        if (opts.leftContainerClassName) {
            mpLeft.addClass(opts.leftContainerClassName);
        }
        if (opts.rightContainerClassName) {
            mpRight.addClass(opts.rightContainerClassName);
        }
        mpButtonCols.append(btnToRight).append(btnToLeft).append(btnAllToRight).append(btnAllToLeft);
        me.addClass("dual-list-selector-initialized");
        //Append Container
        var wrapper = $('<div class="row container-fluid dual-list-selector-wrapper"></div>');
        //Append left
        var mpLeft = $('<div class="col col-md-5 col-sm-12 col-12 card items-container available-items-container"></div>');
        //Append right
        var mpRight = $('<div class="col col-md-5 col-sm-12 col-12 card items-container selected-items-container"></div>');
        wrapper.append(mpLeft).append(mpButtonCols).append(mpRight);

        btnToRight.on('click', function () {
            selectItems(mpLeft, mpRight, me);
        });
        btnAllToRight.on('click', function () {
            selectAllItems(mpLeft, mpRight, me);
        });
        btnToLeft.on('click', function () {
            deselectItems(mpLeft, mpRight, me);
        });
        btnAllToLeft.on('click', function () {
            deselectAllItems(mpLeft, mpRight, me);
        });

        if ($(me).is('select')) {
            var items = [];
            var selectedItems = [];
            $('option', me).each(function () {
                var value = $(this).attr('value');
                var text = $(this).html();
                items.push({
                    value: value,
                    text: text
                });
                if ($(this).is(':selected')) {
                    selectedItems.push({
                        value: value,
                        text: text
                    });
                }
            });
            me.opts = $.extend(opts, {items: items, selectedItems: selectedItems, selectedIndexes: null});
            opts = me.opts;
        }
        if (opts.items) {
            if (!$.isArray(opts.items)) {
                throw 'Available items must be an array/list';
            }
            $.each(opts.items, function (index, item) {
                var lstItem = $('<div class="list-item"/>');
                lstItem.attr('value', item.value).attr('original-index', index).attr('original-text', item.text);
                if (opts.itemRender) {
                    lstItem.html(opts.itemRender(item.value, item.text));
                } else {
                    lstItem.html(item.text);
                }
                mpLeft.append(lstItem);
                lstItem.on('click', function () {
                    toggleItem(lstItem, opts)
                });
            });
        }
        if (opts.selectedItems) {
            if (!$.isArray(opts.selectedItems)) {
                throw 'Selected items must be an array/list';
            }
            $.each(opts.selectedItems, function (index, item) {
                $('.list-item[value="' + item.value + '"]', mpLeft).addClass('dls-selected');
            });
            moveItems('.dls-selected', mpLeft, mpRight);
        } else if (opts.selectedIndexes) {
            if (!$.isArray(opts.selectedIndexes)) {
                throw "Selected items's indexes must be an array/list";
            }
            $.each(opts.selectedIndexes, function (index, item) {
                $('.list-item[original-index="' + item + '"]', mpLeft).addClass('dls-selected');
            });
            moveItems('.dls-selected', mpLeft, mpRight);
        }

        mpLeft.css('height', opts.itemsContainerHeight);
        mpRight.css('height', opts.itemsContainerHeight);
        me.itemsContainer = mpLeft;
        me.selectedItemsContainer = mpRight;

        me.getSelectedItems = function () {
            getSelectedItems(me);
        };

        me.getSelectedIndexes = function () {
            var selectedIndexes = [];
            $('.list-item', me.selectedItemsContainer).each(function () {
                selectedIndexes.push($(this).attr('original-index'));
            });
            return selectedIndexes;
        };

        me.getSelectedValues = function() {
            var selectedValues = [];
            $('.list-item', me.selectedItemsContainer).each(function () {
                selectedValues.push($(this).attr('value'));
            });
            return selectedValues;
        }

        me.selectValues = function (values) {
            $.each(values, function (index, value) {
                $('.list-item', me.itemsContainer).each(function () {
                    if ($(this).attr('value') === value) {
                        $(this).addClass('dls-selected');
                    }
                });
                moveItems('.dls-selected', me.itemsContainer, me.selectedItemsContainer, me.opts.keepOrder);
            });
        };
        me.selectIndexes = function (indexes) {
            $.each(values, function (idx, selectedIdx) {
                $('.list-item:eq(' + selectedIdx + ')', me.itemsContainer).each(function () {
                    $(this).addClass('dls-selected');
                });
                moveItems('.dls-selected', me.itemsContainer, me.selectedItemsContainer, me.opts.keepOrder);
            });
        };
        var thisId = $.trim($(this).attr('id'));
        wrapper.attr('id', (thisId && thisId.length > 0) ? thisId + '-mp-wrapper' : '');
        if ($(me).is('select')) {
            var container = $(me).parent();
            wrapper.insertBefore(me);
            $(me).css("display", "none");
        } else {
            me.html('').append(wrapper);
        }
        return me;
    };

    function doSelect(selector, mpLeft, mpRight, me) {
        var selectedItemsBefore = getSelectedItems(me);
        moveItems(selector, mpLeft, mpRight, me.opts.keepOrder);
        var selectedItemsAfter = getSelectedItems(me);
        if (me.opts.onchange) {
            var isUndo = me.opts.onchange(selectedItemsAfter, selectedItemsBefore);
            if (isUndo) {
                me.selectValues(selectedItemsBefore);
            }
        }
    }

    function selectItems(mpLeft, mpRight, me) {
        doSelect('.dls-selected', mpLeft, mpRight, me);
    }

    function selectAllItems(mpLeft, mpRight, me) {
        doSelect('.list-item', mpLeft, mpRight, me);
    }

    function deselectItems(mpLeft, mpRight, me) {
        moveItems('.dls-selected', mpRight, mpLeft, me.opts.keepOrder);
    }

    function deselectAllItems(mpLeft, mpRight, me) {
        moveItems('.list-item', mpRight, mpLeft, me.opts.keepOrder);
    }

    function toggleItem(listItem, opts) {
        if (listItem.hasClass('dls-selected')) {
            listItem.removeClass('dls-selected');
        } else {
            if (!opts.multipleSelect) {
                var container = $(listItem).closest('.items-container');
                $('.list-item', container).removeClass('dls-selected');
            }
            listItem.addClass('dls-selected');
        }
    }

    function moveItems(selector, sourceContainer, destinationContainer, isKeepOrder) {
        $(selector, sourceContainer).each(function () {
            var movingItem = this;
            $(movingItem).detach().removeClass('dls-selected');
            destinationContainer.append(this);
            if (isKeepOrder) {
                $(".list-item", destinationContainer).sort(function (a, b) {
                    return parseInt($(a).attr('original-index')) > parseInt($(b).attr('original-index'));
                }).each(function () {
                    var elem = $(this);
                    elem.detach();
                    $(elem).appendTo(destinationContainer);
                });
            }
        });
    }

    function getSelectedItems(me) {
        var selectedItems = [];
        $('.list-item', me.selectedItemsContainer).each(function () {
            var item = this;
            selectedItems.push({
                value: $(item).attr('value'),
                text: $(item).attr('original-text')
            })
        });
        return selectedItems;
    }
}(jQuery));
